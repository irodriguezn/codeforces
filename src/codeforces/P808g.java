/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeforces;

/**
 *
 * @author alumno
 */
public class P808g {

    //http://codeforces.com/problemset/problem/808/G
    
    public static void main(String[] args) {
        String himno="win??w??w?n";
        String pal="win";
    }
    
    static String encajaEnPos(String himno, int pos, String palabra) {
        //Se le pasa el himno y una posición con interrogante
        //Si encaja devuelve qué letra encaja de la palabra.
        //Si no encaja devuelve cadenaVacía
        String letra="";
        boolean encaja=false;
        boolean encajaIzquierda=false;
        boolean encajaDerecha=false;
        //Voy a ir probando a meter la palabra empezando por el último caracter
        //para ver si se puede encajar solapando por la izquierda.
        //por ejemplo si el himno es "wi???n" la posición de la interrogante es 2
        //y la palabra es "win", voy a probar a colocar la palabra sustituyendo
        //el interrogante por "n". En este caso tendría éxito y la letra a colo-
        //car sería precisamente la "n". Si hubiera empezado a probar colocando
        //en esa posición la w también tendría éxito pero perdería el
        //solapamiento por la izquierda. Colocaría: wiwinn y sólo saldría una
        //ocurrencia de win. Empezando por la n llegaría a winwin que son 2
        //ocurrencias de la palabra.
        for (int i=palabra.length()-1;i>=0;i--) {
            //Tengo que ver si colocando la letra que ocupa la posición i me
            //encajan el resto de letras. Es decir, coinciden o en la original
            //hay interrogantes. Pero me tiene que coincidir tanto por la iz-
            //quierda como por la derecha. Es decir si tengo wi? y empiezo
            //a probar colocando la n veré que tanto la i como la w de la iz-
            //quierda me coinciden. Si fuera w? y meto la "n" de "win" en el 
            //interrogante y veo si encaja la i no coincidiría con la w del 
            //himno "w?" así que no encaja y debería probar colocando la i en 
            //lugar de la n. Entonces vería que por la izquierda si que encaja.
            //pero puede que por la derecha no. Eso también he de comprobarlo.
            //está claro que sustituyendo por i la ? me coincide la w con la w
            //pero por la derecha depende de lo que haya. Si el himno es w? y 
            //no sigue, está claro que no cabe, porque la n de la palabra no me
            //cabe en el himno.
            int h=pos;
            //Vamos a mirar primero por la izquierda.
            encajaIzquierda=true;
            for (int j=i-1;j<=0; j--) {
                h--;
                if (!(himno.charAt(h)+"").equalsIgnoreCase(letra.charAt(j)+"")) {
                    encajaIzquierda=false;
                }
                
            }
        }    
        
        
        return letra;
    }
    
}
